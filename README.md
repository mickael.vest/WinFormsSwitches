# WinFormsSwitches
### Modern toggle switch controls for Windows Forms applications.
![](winformsswitches.gif)

A class library containing the following :
 - A base class `SwitchBase` that can be used to create any number of new controls. It includes the toggle behavior and is easily set with the right images for the job
 - Five controls, inherited from the `SwitchBase` using different kind of switches. They make use of simple PNGs.

### Download
The latest release can be downloaded it here : 
https://gitlab.com/mickael.vest/WinFormsSwitches/uploads/21237e1b1279c67e0c49822b4789dff9/WinFormsSwitches_1.0_AnyCPU.zip

NuGet : Coming Soon

### Usage
 1. Add the library to your project as a reference
 2. Build your project/solution ; the controls should show up in the toolbox of the designer
 3. Add your control to the form
 4. Attach something to the events you need

The properties to be used are :
 - `ImageOn` : The image you want to use for the *ON* state
 - `ImageOff` : The image you want to use for the *OFF* state
 - `ImageHoverOn` : The image you want to use when the mouse hovers and the switch is *ON*
 - `ImageHoverOff` : The image you want to use when the mouse hovers and the switch is *OFF*
 - `State` : Value from the enum `ToggleStates` which shows if the switch is *ON* or *OFF*

There are also 3 events :

 - `StateChanged` : Fires whenever a change of state occurs
 - `SwitchedOn` : Fires when the switch gets to the state *ON*
 - `SwitchedOff` : Fires when the switch gets to the state *OFF*

### Extensibility
It is very easy to create new controls. All you need is to add a custom control (to the library or directly your project) and make it inherit `SwitchBase`.
You also need to add the corresponding images to the resources of your project.
Finally inherit the constructor of `SwitchBase`, passing it the images. For example this is how the PushOnOff switch is built :

```csharp
    public partial class SwitchPushOnOff : SwitchBase
    {
      public SwitchPushOnOff() : base(Properties.Resources._4_on, Properties.Resources._4_off)
      {
        InitializeComponent();
      }
     }
```

### Limitations
One big limitation right now is the sizing. The controls should be the size of the used PNG.
Out of the box controls are 150x150.

### Credits
Images for the controls from https://www.freepik.com/free-psd/toggle-switches-pack_678930.htm#term=toggle&page=1&position=1

Icon for the demo app from https://www.iconfinder.com/icons/227555/switch_icon#size=512