﻿namespace WinFormsSwitches.Demo
{
    partial class MainForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.switchPushOnOff1 = new WinFormsSwitches.SwitchPushOnOff();
            this.switchSlideLight1 = new WinFormsSwitches.SwitchSlideLight();
            this.switchSlideOnOffRound1 = new WinFormsSwitches.SwitchSlideOnOffRound();
            this.label3 = new System.Windows.Forms.Label();
            this.switchSlideOnOffSquare1 = new WinFormsSwitches.SwitchSlideOnOffSquare();
            this.label4 = new System.Windows.Forms.Label();
            this.switchSlideOuiNon1 = new WinFormsSwitches.SwitchSlideOuiNon();
            this.label5 = new System.Windows.Forms.Label();
            this.lblStatePushOnOff = new System.Windows.Forms.Label();
            this.lblStateSlideLight = new System.Windows.Forms.Label();
            this.lblStateSlideOnOffRound = new System.Windows.Forms.Label();
            this.lblStateSlideOnOffSquare = new System.Windows.Forms.Label();
            this.lblStateSlideOuiNon = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "PushOnOff";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(191, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "SlideLight";
            // 
            // switchPushOnOff1
            // 
            this.switchPushOnOff1.ImageHoverOff = null;
            this.switchPushOnOff1.ImageHoverOn = null;
            this.switchPushOnOff1.ImageOff = ((System.Drawing.Image)(resources.GetObject("switchPushOnOff1.ImageOff")));
            this.switchPushOnOff1.ImageOn = ((System.Drawing.Image)(resources.GetObject("switchPushOnOff1.ImageOn")));
            this.switchPushOnOff1.Location = new System.Drawing.Point(12, 12);
            this.switchPushOnOff1.Name = "switchPushOnOff1";
            this.switchPushOnOff1.Size = new System.Drawing.Size(114, 55);
            this.switchPushOnOff1.State = WinFormsSwitches.ToggleStates.OFF;
            this.switchPushOnOff1.TabIndex = 2;
            this.switchPushOnOff1.StateChanged += new System.EventHandler(this.switchPushOnOff1_StateChanged);
            // 
            // switchSlideLight1
            // 
            this.switchSlideLight1.ImageHoverOff = ((System.Drawing.Image)(resources.GetObject("switchSlideLight1.ImageHoverOff")));
            this.switchSlideLight1.ImageHoverOn = ((System.Drawing.Image)(resources.GetObject("switchSlideLight1.ImageHoverOn")));
            this.switchSlideLight1.ImageOff = ((System.Drawing.Image)(resources.GetObject("switchSlideLight1.ImageOff")));
            this.switchSlideLight1.ImageOn = ((System.Drawing.Image)(resources.GetObject("switchSlideLight1.ImageOn")));
            this.switchSlideLight1.Location = new System.Drawing.Point(200, 24);
            this.switchSlideLight1.Name = "switchSlideLight1";
            this.switchSlideLight1.Size = new System.Drawing.Size(62, 32);
            this.switchSlideLight1.State = WinFormsSwitches.ToggleStates.OFF;
            this.switchSlideLight1.TabIndex = 3;
            this.switchSlideLight1.StateChanged += new System.EventHandler(this.switchSlideLight1_StateChanged);
            // 
            // switchSlideOnOffRound1
            // 
            this.switchSlideOnOffRound1.ImageHoverOff = ((System.Drawing.Image)(resources.GetObject("switchSlideOnOffRound1.ImageHoverOff")));
            this.switchSlideOnOffRound1.ImageHoverOn = ((System.Drawing.Image)(resources.GetObject("switchSlideOnOffRound1.ImageHoverOn")));
            this.switchSlideOnOffRound1.ImageOff = ((System.Drawing.Image)(resources.GetObject("switchSlideOnOffRound1.ImageOff")));
            this.switchSlideOnOffRound1.ImageOn = ((System.Drawing.Image)(resources.GetObject("switchSlideOnOffRound1.ImageOn")));
            this.switchSlideOnOffRound1.Location = new System.Drawing.Point(366, 17);
            this.switchSlideOnOffRound1.Name = "switchSlideOnOffRound1";
            this.switchSlideOnOffRound1.Size = new System.Drawing.Size(94, 47);
            this.switchSlideOnOffRound1.State = WinFormsSwitches.ToggleStates.OFF;
            this.switchSlideOnOffRound1.TabIndex = 4;
            this.switchSlideOnOffRound1.StateChanged += new System.EventHandler(this.switchSlideOnOffRound1_StateChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(344, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 21);
            this.label3.TabIndex = 5;
            this.label3.Text = "SlideOnOffRound";
            // 
            // switchSlideOnOffSquare1
            // 
            this.switchSlideOnOffSquare1.ImageHoverOff = ((System.Drawing.Image)(resources.GetObject("switchSlideOnOffSquare1.ImageHoverOff")));
            this.switchSlideOnOffSquare1.ImageHoverOn = ((System.Drawing.Image)(resources.GetObject("switchSlideOnOffSquare1.ImageHoverOn")));
            this.switchSlideOnOffSquare1.ImageOff = ((System.Drawing.Image)(resources.GetObject("switchSlideOnOffSquare1.ImageOff")));
            this.switchSlideOnOffSquare1.ImageOn = ((System.Drawing.Image)(resources.GetObject("switchSlideOnOffSquare1.ImageOn")));
            this.switchSlideOnOffSquare1.Location = new System.Drawing.Point(101, 159);
            this.switchSlideOnOffSquare1.Name = "switchSlideOnOffSquare1";
            this.switchSlideOnOffSquare1.Size = new System.Drawing.Size(94, 47);
            this.switchSlideOnOffSquare1.State = WinFormsSwitches.ToggleStates.OFF;
            this.switchSlideOnOffSquare1.TabIndex = 6;
            this.switchSlideOnOffSquare1.StateChanged += new System.EventHandler(this.switchSlideOnOffSquare1_StateChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(81, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 21);
            this.label4.TabIndex = 7;
            this.label4.Text = "SlideOnOffSquare";
            // 
            // switchSlideOuiNon1
            // 
            this.switchSlideOuiNon1.ImageHoverOff = ((System.Drawing.Image)(resources.GetObject("switchSlideOuiNon1.ImageHoverOff")));
            this.switchSlideOuiNon1.ImageHoverOn = ((System.Drawing.Image)(resources.GetObject("switchSlideOuiNon1.ImageHoverOn")));
            this.switchSlideOuiNon1.ImageOff = ((System.Drawing.Image)(resources.GetObject("switchSlideOuiNon1.ImageOff")));
            this.switchSlideOuiNon1.ImageOn = ((System.Drawing.Image)(resources.GetObject("switchSlideOuiNon1.ImageOn")));
            this.switchSlideOuiNon1.Location = new System.Drawing.Point(328, 157);
            this.switchSlideOuiNon1.Name = "switchSlideOuiNon1";
            this.switchSlideOuiNon1.Size = new System.Drawing.Size(119, 55);
            this.switchSlideOuiNon1.State = WinFormsSwitches.ToggleStates.OFF;
            this.switchSlideOuiNon1.TabIndex = 8;
            this.switchSlideOuiNon1.StateChanged += new System.EventHandler(this.switchSlideOuiNon1_StateChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(341, 214);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 21);
            this.label5.TabIndex = 9;
            this.label5.Text = "SlideOuiNon";
            // 
            // lblStatePushOnOff
            // 
            this.lblStatePushOnOff.AutoSize = true;
            this.lblStatePushOnOff.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatePushOnOff.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblStatePushOnOff.Location = new System.Drawing.Point(46, 91);
            this.lblStatePushOnOff.Name = "lblStatePushOnOff";
            this.lblStatePushOnOff.Size = new System.Drawing.Size(38, 21);
            this.lblStatePushOnOff.TabIndex = 10;
            this.lblStatePushOnOff.Text = "OFF";
            // 
            // lblStateSlideLight
            // 
            this.lblStateSlideLight.AutoSize = true;
            this.lblStateSlideLight.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStateSlideLight.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblStateSlideLight.Location = new System.Drawing.Point(212, 91);
            this.lblStateSlideLight.Name = "lblStateSlideLight";
            this.lblStateSlideLight.Size = new System.Drawing.Size(38, 21);
            this.lblStateSlideLight.TabIndex = 11;
            this.lblStateSlideLight.Text = "OFF";
            // 
            // lblStateSlideOnOffRound
            // 
            this.lblStateSlideOnOffRound.AutoSize = true;
            this.lblStateSlideOnOffRound.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStateSlideOnOffRound.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblStateSlideOnOffRound.Location = new System.Drawing.Point(391, 91);
            this.lblStateSlideOnOffRound.Name = "lblStateSlideOnOffRound";
            this.lblStateSlideOnOffRound.Size = new System.Drawing.Size(38, 21);
            this.lblStateSlideOnOffRound.TabIndex = 12;
            this.lblStateSlideOnOffRound.Text = "OFF";
            // 
            // lblStateSlideOnOffSquare
            // 
            this.lblStateSlideOnOffSquare.AutoSize = true;
            this.lblStateSlideOnOffSquare.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStateSlideOnOffSquare.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblStateSlideOnOffSquare.Location = new System.Drawing.Point(128, 235);
            this.lblStateSlideOnOffSquare.Name = "lblStateSlideOnOffSquare";
            this.lblStateSlideOnOffSquare.Size = new System.Drawing.Size(38, 21);
            this.lblStateSlideOnOffSquare.TabIndex = 13;
            this.lblStateSlideOnOffSquare.Text = "OFF";
            // 
            // lblStateSlideOuiNon
            // 
            this.lblStateSlideOuiNon.AutoSize = true;
            this.lblStateSlideOuiNon.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStateSlideOuiNon.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblStateSlideOuiNon.Location = new System.Drawing.Point(374, 235);
            this.lblStateSlideOuiNon.Name = "lblStateSlideOuiNon";
            this.lblStateSlideOuiNon.Size = new System.Drawing.Size(38, 21);
            this.lblStateSlideOuiNon.TabIndex = 14;
            this.lblStateSlideOuiNon.Text = "OFF";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 281);
            this.Controls.Add(this.lblStateSlideOuiNon);
            this.Controls.Add(this.lblStateSlideOnOffSquare);
            this.Controls.Add(this.lblStateSlideOnOffRound);
            this.Controls.Add(this.lblStateSlideLight);
            this.Controls.Add(this.lblStatePushOnOff);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.switchSlideOuiNon1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.switchSlideOnOffSquare1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.switchSlideOnOffRound1);
            this.Controls.Add(this.switchSlideLight1);
            this.Controls.Add(this.switchPushOnOff1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "WinFormSwitches Demo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private SwitchPushOnOff switchPushOnOff1;
        private SwitchSlideLight switchSlideLight1;
        private SwitchSlideOnOffRound switchSlideOnOffRound1;
        private System.Windows.Forms.Label label3;
        private SwitchSlideOnOffSquare switchSlideOnOffSquare1;
        private System.Windows.Forms.Label label4;
        private SwitchSlideOuiNon switchSlideOuiNon1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblStatePushOnOff;
        private System.Windows.Forms.Label lblStateSlideLight;
        private System.Windows.Forms.Label lblStateSlideOnOffRound;
        private System.Windows.Forms.Label lblStateSlideOnOffSquare;
        private System.Windows.Forms.Label lblStateSlideOuiNon;
    }
}

