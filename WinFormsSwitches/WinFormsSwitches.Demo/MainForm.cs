﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormsSwitches.Demo
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void switchPushOnOff1_StateChanged(object sender, EventArgs e)
        {
            lblStatePushOnOff.Text = (switchPushOnOff1.State == ToggleStates.ON ? "ON" : "OFF");
        }

        private void switchSlideLight1_StateChanged(object sender, EventArgs e)
        {
            lblStateSlideLight.Text = (switchSlideLight1.State == ToggleStates.ON ? "ON" : "OFF");
        }

        private void switchSlideOnOffRound1_StateChanged(object sender, EventArgs e)
        {
            lblStateSlideOnOffRound.Text = (switchSlideOnOffRound1.State == ToggleStates.ON ? "ON" : "OFF");
        }

        private void switchSlideOnOffSquare1_StateChanged(object sender, EventArgs e)
        {
            lblStateSlideOnOffSquare.Text = (switchSlideOnOffSquare1.State == ToggleStates.ON ? "ON" : "OFF");
        }

        private void switchSlideOuiNon1_StateChanged(object sender, EventArgs e)
        {
            lblStateSlideOuiNon.Text = (switchSlideOuiNon1.State == ToggleStates.ON ? "ON" : "OFF");
        }
    }
}
