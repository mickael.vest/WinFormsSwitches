﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WinFormsSwitches
{
    /// <summary>
    /// Possible states for the switch
    /// </summary>
    public enum ToggleStates
    {
        ON,
        OFF
    }
}
