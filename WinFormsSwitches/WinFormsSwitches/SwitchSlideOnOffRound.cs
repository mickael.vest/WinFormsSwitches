﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormsSwitches
{
    public partial class SwitchSlideOnOffRound : SwitchBase
    {
        public SwitchSlideOnOffRound() : base(Properties.Resources._2_on, Properties.Resources._2_off)
        {
            ImageHoverOff = Properties.Resources._2_off_over;
            ImageHoverOn = Properties.Resources._2_on_over;

            InitializeComponent();
        }
    }
}
