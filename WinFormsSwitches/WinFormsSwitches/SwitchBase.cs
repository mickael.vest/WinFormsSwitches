﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace WinFormsSwitches
{
    public partial class SwitchBase : UserControl
    {
        /// <summary>
        /// Image to show when the switch is ON
        /// </summary>
        public Image ImageOn { get; set; }

        /// <summary>
        /// Image to show when the switch is OFF
        /// </summary>
        public Image ImageOff { get; set; }

        /// <summary>
        /// Hovering image when the switch is ON
        /// </summary>
        public Image ImageHoverOn { get; set; }

        /// <summary>
        /// Hovering image when the switch is OFF
        /// </summary>
        public Image ImageHoverOff { get; set; }

        /// <summary>
        /// Event fired when the state of the switch changes
        /// </summary>
        public event EventHandler StateChanged;
        public event EventHandler SwitchedOn;
        public event EventHandler SwitchedOff;

        /// <summary>
        /// Indicates if the switch has the same hovering whether it's ON or OFF
        /// </summary>
        protected bool HasSameHoverOnOff { get; set; }

        /// <summary>
        /// State of the switch
        /// </summary>
        public ToggleStates State
        {
            get { return _state; }
            set
            {
                if (value == ToggleStates.ON)
                    img.Image = ImageOn;
                else
                    img.Image = ImageOff;

                _state = value;
            }
        }
        private ToggleStates _state = ToggleStates.OFF;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="on">Image to be used when the switch is ON</param>
        /// <param name="off">Image to be used when the switch is OFF</param>
        public SwitchBase(Image on, Image off)
        {
            InitializeComponent();

            ImageOn = on;
            ImageOff = off;

            Height = on.Height;
            Width = on.Width;

            img.Image = ImageOff;
        }

        /// <summary>
        /// Toggles the switch to opposite position
        /// </summary>
        public void SwitchState()
        {            
            if (State == ToggleStates.OFF)
            {
                State = ToggleStates.ON;
                SwitchedOn?.Invoke(this, new EventArgs());
            }
            else
            {
                State = ToggleStates.OFF;
                SwitchedOff?.Invoke(this, new EventArgs());
            }

            StateChanged?.Invoke(this, new EventArgs());
        }

        private void Img_MouseEnter(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;

            if ((State == ToggleStates.ON) && (ImageHoverOn != null))
                img.Image = ImageHoverOn;
            else if (ImageHoverOff != null)
                img.Image = ImageHoverOff;
        }

        private void Img_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Arrow;
            if (State == ToggleStates.ON)
                img.Image = ImageOn;
            else
                img.Image = ImageOff;
        }

        private void Img_Click(object sender, EventArgs e)
        {
            SwitchState();

            // Since the state changed, if the hover images are different
            // We provoke the change of hovering image
            if ((ImageHoverOn != null) && (ImageHoverOff != null) && !HasSameHoverOnOff)
                Img_MouseEnter(this, e);
        }
    }
}
