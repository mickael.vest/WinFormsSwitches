﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormsSwitches
{
    public partial class SwitchSlideOnOffSquare : SwitchBase
    {
        public SwitchSlideOnOffSquare() : base(Properties.Resources._1_on, Properties.Resources._1_off)
        {
            ImageHoverOff = Properties.Resources._1_off_over;
            ImageHoverOn = Properties.Resources._1_on_over;

            InitializeComponent();
        }
    }
}
