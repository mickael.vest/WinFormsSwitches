﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormsSwitches
{
    public partial class SwitchPushOnOff : SwitchBase
    {
        public SwitchPushOnOff() : base(Properties.Resources._4_on, Properties.Resources._4_off)
        {
            InitializeComponent();
        }
    }
}
