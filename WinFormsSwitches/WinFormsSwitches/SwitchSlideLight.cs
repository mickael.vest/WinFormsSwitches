﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormsSwitches
{
    public partial class SwitchSlideLight : SwitchBase
    {
        public SwitchSlideLight() : base(Properties.Resources._7_on, Properties.Resources._7_off)
        {
            ImageHoverOn = Properties.Resources._7_on_over;
            ImageHoverOff = Properties.Resources._7_off_over;

            InitializeComponent();
        }
    }
}
