﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormsSwitches
{
    public partial class SwitchSlideOuiNon : SwitchBase
    {
        public SwitchSlideOuiNon() : base(Properties.Resources._5_oui, Properties.Resources._5_non)
        {
            ImageHoverOff = Properties.Resources._5_non_over;
            ImageHoverOn = Properties.Resources._5_oui_over;

            InitializeComponent();
        }
    }
}
